# AZRS-tracking

**Repozitorijum namenjen izradi projekta iz predmeta Alati za razvoj softvera**

[Projekat iz RS-a](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/18-knights-of-rendlion-board-game)

[Kanban tabla](https://gitlab.com/vucicevic/azrs-tracking/-/boards/2271644?assignee_username=vucicevic&)

Sve što je dokumentovano je stavljeno u opis Issue-a, dok se u komentaru nalazi samo link ka commit-u ili potrebnim fajlovima...
